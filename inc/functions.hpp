#pragma once
#define N 100
namespace f
{
	bool Find35(int a);
	int Read(int(&matrix)[N][N]);
	void Write(int matrix[N][N],int n);
	void Change(int(&matrix)[N][N],int n);

}