#pragma once
#include<iostream>
#include<fstream>
#include<string>
#include"functions.hpp"
#define N 100
using namespace std;

namespace f
{
	bool find35(int a)
	{
		while (a != 0)
		{
			if (a % 10 == 3)
			{
				return true;
			}
			else if (a % 10 == 5)
			{
				return true;
			}
			a %= 10;
		}
		return false;
	}
	int Read(int(&matrix)[N][N])
	{
		int n;
		ifstream in("input.txt");
		in >> n;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				in >> matrix[i][j];
		return n;
	}
	void Write(int matrix[N][N], int n)
	{
		ofstream out("output.txt");
		for (int i = 0; i < n; i++)
		{
			out << endl;
			for (int j = 0; j < n; j++)
				out << matrix[i][j] << ' ';


		}

	}
	void Change(int(&matrix)[N][N], int n)
	{
		int sum = 0;
		for (int j = 0; j < n; j++)
		{
			int min1 = 0;
			int min = INT_MAX;
			for (int i = 0; i < n; i++)
			{
				if (matrix[i][j] < min)
				{
					min = matrix[i][j];
					min1 = i;
				}

			}
			for (int i = 0; i < n; i++)
				sum += (matrix[i][j] * matrix[i][j]);
			matrix[min1][j] = sum;
		}
	}
}
